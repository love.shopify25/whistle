class CartDrawer extends HTMLElement {
  constructor() {
    super();
    this.addEventListener('keyup', (evt) => evt.code === 'Escape' && this.close());
    this.querySelector('#CartDrawer-Overlay').addEventListener('click', this.close.bind(this));
    this.setHeaderCartIconAccessibility();
    this.addEventListener('click',function(e){
      if(e.target){
        if(e.target.getAttribute('data-el') == 'addToCart'){
          let productUpSell = e.target.closest('product-up-sell');
          if(productUpSell){
            const body = JSON.stringify({
              id : productUpSell.getAttribute('variant-id'),
              quantity:1,
              sections: document.querySelector('cart-drawer').querySelector('cart-drawer-items').getSectionsToRender().map((section) => section.section),
              sections_url: window.location.pathname
            });
            fetch(`${routes.cart_url}/add.js`, { ...fetchConfig(), ...{ body } })
            .then((response) => response.json())
            .then((response) => {
              document.querySelector('cart-drawer').querySelector('cart-drawer-items').getSectionsToRender().forEach((section => {
                const elementToReplace =
                  document.getElementById(section.id).querySelector(section.selector) || document.getElementById(section.id);
                elementToReplace.innerHTML =
                document.querySelector('cart-drawer').querySelector('cart-drawer-items').getSectionInnerHTML(response.sections[section.section], section.selector);
              }));
              document.querySelector('cart-drawer-items').changeTipProduct();
            })
          }
        }
      }
    })
  }

  setHeaderCartIconAccessibility() {
    const cartLink = document.querySelector('#cart-icon-bubble');
    cartLink.setAttribute('role', 'button');
    cartLink.setAttribute('aria-haspopup', 'dialog');
   
    cartLink.addEventListener('click', (event) => {
      event.preventDefault();
      this.open(cartLink)
    });
    cartLink.addEventListener('keydown', (event) => {
      if (event.code.toUpperCase() === 'SPACE') {
        event.preventDefault();
        this.open(cartLink);
      }
    });
    // user defined
    const mobileCartLink = document.querySelector('#cart-icon-bubble-mobile');
    mobileCartLink.setAttribute('role','button');
    mobileCartLink.setAttribute('aria-haspopup', 'dialog');
    mobileCartLink.addEventListener('click',(event) => {
      event.preventDefault();
      this.open(mobileCartLink);
    })
    mobileCartLink.addEventListener('keydown', (event) => {
      if (event.code.toUpperCase() === 'SPACE') {
        event.preventDefault();
        this.open(mobileCartLink);
      }
    });
  }
  open(triggeredBy) {
    if (triggeredBy) this.setActiveElement(triggeredBy);
    const cartDrawerNote = this.querySelector('[id^="Details-"] summary');
    if (cartDrawerNote && !cartDrawerNote.hasAttribute('role')) this.setSummaryAccessibility(cartDrawerNote);
    // here the animation doesn't seem to always get triggered. A timeout seem to help
    setTimeout(() => {this.classList.add('animate', 'active')});
    this.addEventListener('transitionend', () => {
      const containerToTrapFocusOn = this.classList.contains('is-empty') ? this.querySelector('.drawer__inner-empty') : document.getElementById('CartDrawer');
      const focusElement = this.querySelector('.drawer__inner') || this.querySelector('.drawer__close');
      trapFocus(containerToTrapFocusOn, focusElement);
    }, { once: true });

    document.body.classList.add('overflow-hidden');
  }

  close() {
    this.classList.remove('active');
    removeTrapFocus(this.activeElement);
    document.body.classList.remove('overflow-hidden');
  }
 
  setSummaryAccessibility(cartDrawerNote) {
    cartDrawerNote.setAttribute('role', 'button');
    cartDrawerNote.setAttribute('aria-expanded', 'false');

    if(cartDrawerNote.nextElementSibling.getAttribute('id')) {
      cartDrawerNote.setAttribute('aria-controls', cartDrawerNote.nextElementSibling.id);
    }

    cartDrawerNote.addEventListener('click', (event) => {
      event.currentTarget.setAttribute('aria-expanded', !event.currentTarget.closest('details').hasAttribute('open'));
    });
    cartDrawerNote.parentElement.addEventListener('keyup', onKeyUpEscape);
  }

  renderContents(parsedState) {
    this.querySelector('.drawer__inner').classList.contains('is-empty') && this.querySelector('.drawer__inner').classList.remove('is-empty');
    this.productId = parsedState.id;
    this.getSectionsToRender().forEach((section => {
      const sectionElement = section.selector ? document.querySelector(section.selector) : document.getElementById(section.id);
      sectionElement.innerHTML =
          this.getSectionInnerHTML(parsedState.sections[section.id], section.selector);
    }));
    
    setTimeout(() => {
      this.querySelector('#CartDrawer-Overlay').addEventListener('click', this.close.bind(this));
      this.open();
    });
  }

  getSectionInnerHTML(html, selector = '.shopify-section') {
    return new DOMParser()
      .parseFromString(html, 'text/html')
      .querySelector(selector).innerHTML;
  }

  getSectionsToRender() {
    return [
      {
        id: 'cart-drawer',
        selector: '#CartDrawer'
      },
      {
        id: 'cart-icon-bubble'
      }
    ];
  }

  getSectionDOM(html, selector = '.shopify-section') {
    return new DOMParser()
      .parseFromString(html, 'text/html')
      .querySelector(selector);
  }

  setActiveElement(element) {
    this.activeElement = element;
  }
}
customElements.define('cart-drawer', CartDrawer);

class CartDrawerItems extends CartItems {
  cartDrawerData = [];
 
  constructor(){
    super();
    this.changeVariant();
  }

  getSectionsToRender() {
    return [
      {
        id: 'CartDrawer',
        section: 'cart-drawer',
        selector: '.drawer__inner'
      },
      {
        id: 'cart-icon-bubble',
        section: 'cart-icon-bubble',
        selector: '.shopify-section'
      }
    ];
  }


  // custom defined
  onChange(event) {
    if(event.target.dataset.index != null){
      this.updateQuantity(event.target.dataset.index, event.target.value, document.activeElement.getAttribute('name'));
    }
  }
 
  changeVariant(){
    this.querySelectorAll('.cart-item').forEach((cartItem) => {
      cartItem.querySelector('.product-edit-button').addEventListener('click',(event)=>{
        if(event.target.dataset.el == 'btnEdit'){
          event.target.dataset.el = 'btnUpdate';
          event.target.innerHTML = 'Update';
          cartItem.querySelector('.quantity-wrapper').classList.add('hide');
          cartItem.querySelector('.current-item-option').classList.add('hide');
          cartItem.querySelector('.variant-select-wrapper').classList.remove('hide');
        }else{
          event.target.dataset.el = 'btnEdit';
          event.target.innerHTML = 'Edit';
          cartItem.querySelector('.quantity-wrapper').classList.remove('hide');
          cartItem.querySelector('.current-item-option').classList.remove('hide');
          cartItem.querySelector('.variant-select-wrapper').classList.add('hide');
          const line = event.target.dataset.index;
          const qty = cartItem.querySelector('.quantity__input').value;
          //OPTIONS
          let fieldsets = Array.from(cartItem.querySelectorAll('fieldset'));
          let options = fieldsets.map((fieldset)=>{
            return Array.from(fieldset.querySelectorAll('input')).find((radio)=> radio.checked).value;
          })
          if(cartItem.querySelectorAll('.select-size-option').length > 0){
            let sizeElements = Array.from(cartItem.querySelectorAll('.select-size-option'));
            let size_option = sizeElements.find((e)=> e.classList.contains('is_checked')).getAttribute('data-size');
            options.push(size_option);
          }
          //current variant change
          let variantData = JSON.parse(cartItem.querySelector('[item-variants-json]').textContent);
          const currentVariant = variantData.find((variant)=>{
            return !variant.options.map((option,index)=>
              {
                for (let index = 0; index < options.length; index++) {
                  if(options[index] == option) return true;
                }
                return false;
              }
            ).includes(false)
          })
          if(event.target.dataset.vid != currentVariant.id && currentVariant.available){
            event.target.dataset.vid = currentVariant.id;
            this.updateCart(line,qty,currentVariant.id);
          }
        }
      })
      if(cartItem.querySelector('.accordion-control')){
        cartItem.querySelector('.accordion-control').addEventListener('click',function(){
          if(this.classList.contains('active')){
            cartItem.querySelector('.size-select.is_open').classList.remove('is_open');
            cartItem.querySelector('.accordion-control.active').classList.remove('active');
          }else{
            this.classList.toggle('active');
            cartItem.querySelector('.size-select').classList.add('is_open');
          }
        })
        if(cartItem.querySelectorAll('.select-size-option').length > 0){
          cartItem.querySelectorAll('.select-size-option').forEach(function(element){
            element.addEventListener('click',function(){
              cartItem.querySelector('li.select-size-option.is_checked').classList.remove('is_checked');
              if(cartItem.querySelector('.accordion-control.active').classList.contains('active')){
                cartItem.querySelector('.accordion-control.active').classList.remove('active');
              }
              if( cartItem.querySelector('.size-select.is_open').classList.contains('is_open')){
                cartItem.querySelector('.size-select.is_open').classList.remove('is_open');
              }
              element.classList.add('is_checked');
              cartItem.querySelector('.current-size-label span').innerHTML = this.getAttribute('data-size');
            })
          })
        }
      }
    })
  }
  updateCart(pLine,pQty,pVid){
    let cartDrawItem = this;
    let updateQty = parseInt(pQty);
    $.ajax({
      type:'POST',
      url: `${routes.cart_url}/clear.js`,
      data:'',
      dataType:'json',
      success:function(response){
          cartDrawItem.querySelectorAll('.cart-item').forEach((cartItem,index)=>{
          const vid   = cartItem.querySelector('.product-edit-button').dataset.vid;
          let quantity= parseInt(cartItem.querySelector('.quantity__input').value);
          cartDrawItem.cartDrawerData.push({id:vid,quantity:quantity});
        })
        const body = JSON.stringify({
          items : cartDrawItem.cartDrawerData.reverse(),
          sections: cartDrawItem.getSectionsToRender().map((section) => section.section),
          sections_url: window.location.pathname
        });
        fetch(`${routes.cart_url}/add.js`, { ...fetchConfig(), ...{ body } })
        .then((response) => response.json())
        .then((response) => {
          cartDrawItem.getSectionsToRender().forEach((section => {
            const elementToReplace =
              document.getElementById(section.id).querySelector(section.selector) || document.getElementById(section.id);
            elementToReplace.innerHTML =
            cartDrawItem.getSectionInnerHTML(response.sections[section.section], section.selector);
          }));
        })
      }
    })
    this.changeTipProduct();
  }
}
customElements.define('cart-drawer-items', CartDrawerItems);

class CartNoteSaveButton extends HTMLElement{
  constructor(){
    super();
    this.addEventListener('click',function(){
      const body = JSON.stringify({note: document.querySelector('#CartDrawer-Note').value});
      fetch(`${routes.cart_update_url}`, { ...fetchConfig(), ...{ body } });
    })
  }
}
customElements.define('cart-note-save-button',CartNoteSaveButton);




 
$(document).on('click','[accordion-control]',function(){
  $(this).toggleClass('active');
  $(this).next().slideToggle();
})