changeStrickHeader();

$(window).scroll(function(){
   changeStrickHeader();
   $('#video-with-text-scroll-auto-play').each(function () {
      videoAutoPlayControl($(this).attr('id'));
   });
})

function changeStrickHeader(){
   let announcementBarHeight = $('.announcement-bar-wrap').innerHeight();
   if($(window).scrollTop() >= announcementBarHeight){
      $('.section-header-wrap').addClass('scrolled');
   }else{
      $('.section-header-wrap').removeClass('scrolled');
   }
}
function videoAutoPlayControl(id_text){
   let comp = $("#"+id_text); 
   let sHeight =  $(window).innerHeight();
   let eHeight = comp.innerHeight();
   let vedeoTagPosition = comp.offset();
   let y = vedeoTagPosition.top;
   let scrolY = $(window).scrollTop();
   var n = y - scrolY - eHeight;
   if( n < 0 && (sHeight - Math.abs(n)) > eHeight / 2){
      // document.getElementById(id_text).play();
   }else{
      document.getElementById(id_text).pause();
   };      
}

// MobileNav
$('#div-menu-drawer-container').on('click',function(){
   $('#mobile-nav').addClass('open');
   $('body').addClass('no-scroll');
})
$('#mob-menu-close-btn').on('click',function(){
   $('#mobile-nav').removeClass('open');
   $('body').removeClass('no-scroll');
})


// mobile megamenu accordion
$('.mob-nav-item.has-megamenu').on('click',function(){
   if(!$(this).hasClass("is-open")){
      $('.mob-nav-item.has-megamenu.is-open').next().slideToggle();
      $(".mob-nav-item.has-megamenu.is-open").toggleClass('is-open');
   }
   $(this).toggleClass('is-open');
   $(this).next().slideToggle();
})

/// product slider
$('.image-slider-wrapper').slick({
   slidesToShow: 1,
   slidesToScroll: 1,
   arrows: false,
   asNavFor: '.image-slider-nav'

});
$('.image-slider-nav').slick({
   slidesToShow: 7,
   slidesToScroll: 1,
   asNavFor: '.image-slider-wrapper',
   arrows: false,
   focusOnSelect: true,
   responsive: [
   {
      breakpoint: 1000,
      settings: {
         slidesToShow: 4,
      }
   }]
});
$(document).on('click','.product-media-wrapper .slide-prev',function(){
   $('.image-slider-nav').slick('slickPrev');
   $('.image-slider-wrapper').slick('slickPrev');
})
$(document).on('click','.product-media-wrapper .slide-next',function(){
   $('.image-slider-nav').slick('slickNext');
   $('.image-slider-wrapper').slick('slickNext');

})

$('#main-product-variant-radio').on('change',function(){
   let slideItems = $(this).parents('.product-wrapper').find('.image-slider-nav .slide-nav-item a');
   for (let index = 0; index < slideItems.length; index++) {
      const element = slideItems[index];
     if($(element).attr('data-vids').indexOf(this.currentVariant.id) > -1){
         $('.image-slider-wrapper').slick('slickGoTo',$(element).attr('data-index'));
     };
   }
})
// product upsell button
$(document).on('click','.upsell-collar',function(){
   $(this).toggleClass('checked');
   if($(this).hasClass('checked')){
      $(this).parents('upsell-product').find('.toAdd').text('(click to remove)');
   }else{
      $(this).parents('upsell-product').find('.toAdd').text('(click to add)'); 
   }
})


// global accordion

$(document).on('click','[accordion-button]',function(){
   if(!$(this).hasClass('is_open')){
      $(this).parents('[accordion-contaner]').find('.is_open').next().slideToggle();
      $(this).parents('[accordion-container]').find('.is_open').removeClass('is_open');
   }
   $(this).toggleClass('is_open');
   $(this).next().slideToggle();
})